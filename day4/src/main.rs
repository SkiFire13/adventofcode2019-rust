use std::fs::read_to_string;
use std::iter::successors;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let (min, max) = parse_input(input)?;
    Ok(find_valid(min, max, |count| count >= 2))
}

fn part2(input: &str) -> Result<usize> {
    let (min, max) = parse_input(input)?;
    Ok(find_valid(min, max, |count| count == 2))
}

fn parse_input(input: &str) -> Result<(u32, u32)> {
    let mut words = input.trim_end().split('-');
    let n1: u32 = words.next()
        .ok_or_else(||"Invalid input".to_owned())?
        .parse::<u32>()
        .map_err(|_| "Cannot parse as a positive integer".to_owned())?;
    let n2 = words.next()
        .ok_or_else(|| "Invalid input".to_owned())?
        .parse::<u32>()
        .map_err(|_| "Cannot parse as a positive integer".to_owned())?;
    Ok((n1, n2))
}

fn find_valid(min: u32, max: u32, count_condition: impl Fn(usize) -> bool) -> usize {
    (std::cmp::max(100_000, min) ..= std::cmp::min(999_999, max))
        .filter(|&n| {
            (n % 111_111 == 0 && count_condition(6)) || 
            {
                let (double, incr) = count_digits_tupled(n)
                    .fold((false, true), |(double, incr), ((digit_1, count_1), (digit_2, count_2))|
                        (
                            double || count_condition(count_1) || count_condition(count_2),
                            incr && digit_1 <= digit_2
                        )
                    );
                double && incr
            }
        })
        .count()
}

fn count_digits_tupled(n: u32) -> impl Iterator<Item = ((u32, usize), (u32, usize))> {
    let mut digits = successors(
        Some(n),
        |&prev| if prev >= 10 { Some(prev / 10) } else { None }
    )
    .map(|i| i % 10);

    let mut next_digit = digits.next();
    let mut next = move || {
        next_digit.take().map(|t| {
            let mut count = 1;
            loop {
                next_digit = digits.next();
                match next_digit {
                    Some(next_digit) if next_digit == t => count += 1,
                    _ => break (t, count),
                }
            }
        })
    };

    successors(
        next().and_then(|v2| next().map(|v1| (v1, v2)) ),
        move |&(v2, _)| next().map(|v1| (v1, v2))
    )
}
