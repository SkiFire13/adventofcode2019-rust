use std::collections::{HashMap, HashSet, VecDeque};
use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<u32> {
    let (walls, portals, start, target) = parse(input)?;

    let mut queue = VecDeque::new();
    let mut seen = HashSet::new();

    queue.push_back((0, start));

    while let Some((distance, point)) = queue.pop_front() {
        if seen.insert(point) {
            if point == target {
                return Ok(distance);
            }
            queue.extend(
                [(0, 1), (0, -1), (1, 0), (-1, 0)].iter().cloned()
                    .map(|(dx, dy)| (point.0 + dx, point.1 + dy))
                    .filter(|new_point| !walls.contains(new_point))
                    .chain(portals.get(&point).cloned().into_iter())
                    .map(|new_point| (distance + 1, new_point))
            );
        }
    }

    return Err("Can't reach target".into())
}

fn part2(input: &str) -> Result<i32> {
    let (walls, portals, start, target) = parse(input)?;

    let (minx, maxx, miny, maxy) = portals.keys()
        .fold((std::isize::MAX, 0, std::isize::MAX, 0), |(minx, maxx, miny, maxy), &(x, y)| {
            (
                std::cmp::min(minx, x),
                std::cmp::max(maxx, x),
                std::cmp::min(miny, y),
                std::cmp::max(maxy, y),
            )
        });
    
    let mut queue = VecDeque::new();
    let mut seen = HashSet::new();

    queue.push_back((0, start, 0));

    while let Some((distance, point, depth)) = queue.pop_front() {
        if seen.insert((point, depth)) {
            if point == target && depth == 0 {
                return Ok(distance);
            }
            queue.extend(
                [(0, 1), (0, -1), (1, 0), (-1, 0)].iter().cloned()
                    .map(|(dx, dy)| (point.0 + dx, point.1 + dy))
                    .filter(|new_point| !walls.contains(new_point))
                    .map(|new_point| (distance + 1, new_point, depth))
                    .chain(
                        portals.get(&point).cloned().into_iter()
                            .map(|new_point| {
                                let new_depth = depth + if 
                                    point.0 == minx ||
                                    point.0 == maxx ||
                                    point.1 == miny ||
                                    point.1 == maxy
                                    { -1 } else { 1 };
                                (distance + 1, new_point, new_depth)
                            })
                            .filter(|&(_, _, new_depth)| new_depth >= 0)
                    )
            );
        }
    }

    return Err("Can't reach target".into())
}

type Point = (isize, isize);
fn parse(input: &str) -> Result<(HashSet<Point>, HashMap<Point, Point>, Point, Point)>{
    let mut walls = HashSet::new();
    let mut routes = HashSet::new();
    let mut chars = HashMap::new();

    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let (x, y) = (x as isize, y as isize);
            match c {
                '#' => { walls.insert((x, y)); },
                '.' => { routes.insert((x, y)); }
                c if c.is_ascii_uppercase() => { 
                    walls.insert((x, y));
                    chars.insert((x, y), c);
                },
                _ => {}
            }
        }
    }

    let mut start = None;
    let mut target = None;
    let mut portals_seen = HashMap::new();
    let mut portals = HashMap::new();

    for (&point, &c1) in chars.iter() {
        let real_portal = [(0, 1), (0, -1), (1, 0), (-1, 0)].iter().cloned()
            .map(|(dx, dy)| (point.0 + dx, point.1 + dy))
            .filter(|point| routes.contains(point))
            .next();
        if let Some(real_portal) = real_portal {
            let (new_point, c2) = [(0, 1), (0, -1), (1, 0), (-1, 0)].iter().cloned()
                .map(|(dx, dy)| (point.0 + dx, point.1 + dy))
                .filter_map(|new_point| chars.get(&new_point).cloned().map(|c2| (new_point, c2)))
                .next()
                .ok_or_else(|| "One letter portals are invalid")?;
            match (c1, c2) {
                ('A', 'A') => start = Some(start.xor(Some(real_portal))
                    .ok_or_else(|| "Multiple starting points found")?),
                ('Z', 'Z') => target = Some(target.xor(Some(real_portal))
                    .ok_or_else(|| "Multiple ending points found")?),
                (c1, c2) => {
                    if let Some(other_portal) = portals_seen.insert(if point < new_point { (c1, c2) } else { (c2, c1) }, real_portal) {
                        portals.insert(real_portal, other_portal);
                        portals.insert(other_portal, real_portal);
                    }
                }
            }
        }
    }

    Ok((walls, portals, start.ok_or_else(|| "No start point found")?, target.ok_or_else(|| "No target point found")?))
}
