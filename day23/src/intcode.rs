use std::collections::VecDeque;
use std::convert::{TryFrom, TryInto};
use std::str::FromStr;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Clone)]
pub struct IntcodeDevice {
    ip: usize,
    relative_offset: isize,
    pub memory: Vec<i64>,
    pub input: VecDeque<i64>,
    pub output: VecDeque<i64>
}

impl IntcodeDevice {
    pub fn execute(&mut self) -> Result<DeviceStatus> {
        loop {
            let opcode = {
                let opcode = self.memory.get(self.ip).cloned().ok_or("No more instructions")?;
                if opcode > 0 { opcode as usize } else { return Err("Negative opcode".into()); }
            };

            let param1 = self.memory.get(self.ip + 1).cloned()
                .ok_or_else(|| format!("Param 1 is required for {}", opcode));
            let param2 = self.memory.get(self.ip + 2).cloned()
                .ok_or_else(|| format!("Param 2 is required for {}", opcode));
            let param3 = self.memory.get(self.ip + 3).cloned()
                .ok_or_else(|| format!("Param 3 is required for {}", opcode));

            use ParamMode::*;
            match Self::parse_opcode(opcode)? {
                (99, [Position, Position, Position]) => return Ok(DeviceStatus::Halt),
                (1, [mode1, mode2, mode3]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?, mode3)?;
                    self.memory[result_addr] = param1 + param2;
                    self.ip += 4;
                },
                (2, [mode1, mode2, mode3]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?, mode3)?;
                    self.memory[result_addr] = param1 * param2;
                    self.ip += 4;
                },
                (3, [mode1, Position, Position]) => {
                    let result_addr = self.check_address(param1?, mode1)?;
                    if let Some(input_value) = self.input.pop_front() {
                        self.memory[result_addr] = input_value;
                        self.ip += 2;
                    } else {
                        return Ok(DeviceStatus::WaitingInput)
                    }
                },
                (4, [mode1, Position, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    self.output.push_back(param1);
                    self.ip += 2;
                },
                (5, [mode1, mode2, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    if param1 != 0 {
                        let addr = self.get_param(param2?, mode2)?;
                        self.ip = self.check_ip(addr)?;
                    } else {
                        self.ip += 3;
                    }
                },
                (6, [mode1, mode2, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    if param1 == 0 {
                        let addr = self.get_param(param2?, mode2)?;
                        self.ip = self.check_ip(addr)?;
                    } else {
                        self.ip += 3;
                    }
                },
                (7, [mode1, mode2, mode3]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?, mode3)?;
                    self.memory[result_addr] = if param1 < param2 { 1 } else { 0 };
                    self.ip += 4;
                },
                (8, [mode1, mode2, mode3]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?, mode3)?;
                    self.memory[result_addr] = if param1 == param2 { 1 } else { 0 };
                    self.ip += 4;
                },
                (9, [mode1, Position, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    self.relative_offset += param1 as isize;
                    self.ip += 2;
                },
                (op_code, modes) => return Err(format!("Invalid opcode {} with modes {:?}", op_code, modes).into())
            }
        }
    }

    fn parse_opcode(opcode: usize) -> Result<(usize, [ParamMode ; 3])> {
        Ok((
            opcode % 100,
            [
                (opcode / 100 % 10).try_into()?,
                (opcode / 1000 % 10).try_into()?,
                (opcode / 10000 % 10).try_into()?
            ]
        ))
    }

    fn check_ip(&self, ip: i64) -> Result<usize> {
        ip.try_into()
            .ok()
            .filter(|&ip| ip < self.memory.len())
            .ok_or_else(|| "Cannot set ip out of bounds".into())
    }

    fn check_address(&mut self, address: i64, mode: ParamMode) -> Result<usize> {
        let real_address = match mode {
            ParamMode::Position => address,
            ParamMode::Relative => address + self.relative_offset as i64,
            ParamMode::Immediate => return Err("Cannot use Immediate mode for indexing".into()),
        };
        real_address.try_into()
            .map(|addr| {
                self.require_memory_set(addr);
                addr
            })
            .map_err(|_| format!("Cannot write to negative address {}", address).into())
    }

    fn get_param(&mut self, param: i64, mode: ParamMode) -> Result<i64> {
        match mode {
            ParamMode::Immediate => Ok(param),
            ParamMode::Relative | ParamMode::Position => self.check_address(param, mode)
                .map(|addr| {
                    self.require_memory_set(addr);
                    self.memory[addr]
                })
                .map_err(|_| "Parameter address out of bounds".into())
        }
    }

    fn require_memory_set(&mut self, address: usize) {
        while self.memory.len() <= address {
            self.memory.push(0);
        }
    }
}

impl FromStr for IntcodeDevice {
    type Err = String;
    fn from_str(input: &str) -> std::result::Result<Self, Self::Err> {
        Ok(Self {
            ip: 0,
            relative_offset: 0,
            input: VecDeque::new(),
            output: VecDeque::new(),
            memory: input
                .trim()
                .split(',')
                .map(|n| n.parse::<i64>()
                    .map_err(|_| format!("Cannot parse {} as a positive integer", n))
                )
                .collect::<std::result::Result<_, _>>()?
        })
    }
}

#[derive(Debug)]
enum ParamMode { Immediate, Position, Relative }
impl TryFrom<usize> for ParamMode {
    type Error = Box<dyn std::error::Error>;
    fn try_from(mode: usize) -> Result<Self> {
        match mode {
            0 => Ok(Self::Position),
            1 => Ok(Self::Immediate),
            2 => Ok(Self::Relative),
            _ => Err("Invalid parameter mode in opcode".into())
        }
    }
}

pub enum DeviceStatus { Halt, WaitingInput }
