mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<i64> {
    let device: IntcodeDevice = input.parse()?;
    let mut devices = (0..50).map(|ip| {
        let mut device = device.clone();
        device.input.push_back(ip);
        device
    }).collect::<Vec<_>>();
    loop {
        for ip in 0..devices.len() {
            if devices[ip].input.len() == 0 {
                devices[ip].input.push_back(-1);
            }
            devices[ip].execute()?;
            let output = devices[ip].output.drain(..).collect::<Vec<_>>();
            for o in output.chunks(3) {
                if o.len() != 3 {
                    return Err("Invalid packet size".into())
                } else if o[0] == 255 {
                    return Ok(o[2])
                } else if o[0] >= 0 && o[0] < 50 {
                    devices[o[0] as usize].input.extend(&o[1..])
                } else {
                    return Err("Invalid ip".into())
                }
            }
        }
    }
}

fn part2(input: &str) -> Result<i64> {
    let device: IntcodeDevice = input.parse()?;
    let mut devices = (0..50).map(|ip| {
        let mut device = device.clone();
        device.input.push_back(ip);
        device
    }).collect::<Vec<_>>();
    let mut sent = true;
    let mut nat = [0, 0];
    let mut nat_seen = std::collections::HashSet::new();
    loop {
        if !sent {
            if nat_seen.insert(nat[1]) {
                devices[0].input.extend(&nat);
            } else {
                return Ok(nat[1])
            }
        }
        sent = false;
        for ip in 0..devices.len() {
            if devices[ip].input.len() == 0 {
                devices[ip].input.push_back(-1);
            }
            devices[ip].execute()?;
            let output = devices[ip].output.drain(..).collect::<Vec<_>>();
            for packet in output.chunks(3) {
                if packet.len() != 3 {
                    return Err("Invalid packet size".into())
                } else if packet[0] == 255 {
                    nat = [packet[1], packet[2]];
                } else if packet[0] >= 0 && packet[0] < 50 {
                    devices[packet[0] as usize].input.extend(&packet[1..])
                } else {
                    return Err("Invalid ip".into())
                }
                sent = true;
            }
        }
    }
}
