use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);
    
    Ok(())
}

fn part1(input: &str) -> Result<i32> {
    let mut moons = parse_input(input)?;

    for _ in 0..1000 {
        for i in 0..moons.len() {
            let current_moon = &moons[i];
            let (dx, dy, dz) = moons.iter().fold((0i32, 0i32, 0i32), |(dx, dy, dz), moon|
                (
                    dx + cmp_i32(moon.x, current_moon.x),
                    dy + cmp_i32(moon.y, current_moon.y),
                    dz + cmp_i32(moon.z, current_moon.z),
                )
            );
            let current_moon = &mut moons[i];
            current_moon.vx += dx;
            current_moon.vy += dy;
            current_moon.vz += dz;
        }
        for moon in moons.iter_mut() {
            moon.x += moon.vx;
            moon.y += moon.vy;
            moon.z += moon.vz;
        }
    }

    Ok(
        moons.iter()
            .map(|moon| (moon.x.abs() + moon.y.abs() + moon.z.abs()) * (moon.vx.abs() + moon.vy.abs() + moon.vz.abs()))
            .sum()
    )
}

fn part2(input: &str) -> Result<u64> {
    let moons = parse_input(input)?;

    let step_x = step_to_repeat(moons.iter().map(|&moon| (moon.x, moon.vx)).collect());
    let step_y = step_to_repeat(moons.iter().map(|&moon| (moon.y, moon.vy)).collect());
    let step_z = step_to_repeat(moons.iter().map(|&moon| (moon.z, moon.vz)).collect());

    Ok(mcm(step_x, mcm(step_y, step_z)))
}

#[derive(PartialEq, Eq, Clone, Copy)]
struct Moon {
    x: i32, y: i32, z: i32,
    vx: i32, vy: i32, vz: i32,
}

fn parse_input(input: &str) -> Result<Vec<Moon>> {
    input.lines()
        .map(|line| {
            let mut split = line.split(&['=', ',', '>'][..]);
            split.next();
            let x = split.next().ok_or_else(|| "Failed to parse x")?.parse().map_err(|_| "No x found")?;
            split.next();
            let y = split.next().ok_or_else(|| "Failed to parse y")?.parse().map_err(|_| "No y found")?;
            split.next();
            let z = split.next().ok_or_else(|| "Failed to parse z")?.parse().map_err(|_| "No z found")?;

            Ok(Moon { x, y, z, vx: 0, vy: 0, vz: 0 })
        })
        .collect()
}

fn step_to_repeat(mut parts: Vec<(i32, i32)>) -> u64 {
    let initial_parts = parts.clone();
    let mut step = 1;
    loop {
        for i in 0..parts.len() {
            parts[i].1 += parts.iter().map(|&(s, _)| cmp_i32(s, parts[i].0)).sum::<i32>();
        }
        for moon_part in parts.iter_mut() {
            moon_part.0 += moon_part.1;
        }
        if initial_parts == parts {
            return step
        }
        step += 1;
    }
}

fn cmp_i32(a: i32, b: i32) -> i32 {
    match a.cmp(&b) {
        std::cmp::Ordering::Greater => 1,
        std::cmp::Ordering::Less => -1,
        std::cmp::Ordering::Equal => 0,
    }
}

fn mcm(a: u64, b: u64) -> u64 {
    let mut x = a;
    let mut y = b;
    while y != 0 {
        std::mem::swap(&mut x, &mut y);
        y = y % x;
    }

    a * b / x
}
