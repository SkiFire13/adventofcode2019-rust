use std::fs::read_to_string;
use std::path::Path;
use std::str::FromStr;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let device = {
        let mut device: IntcodeDevice = input.trim_end().parse()?;
        device.memory[1] = 12;
        device.memory[2] = 2;
        device
    };

    device.execute().ok_or_else(|| "Executor found invalid opcode".into())
}

fn part2(input: &str) -> Result<usize> {
    let device: IntcodeDevice = input.trim_end().parse()?;

    for noun in 0..99 {
        for verb in 0..99 {
            let device = {
                let mut device = device.clone();
                device.memory[1] = noun;
                device.memory[2] = verb;
                device
            };
            if let Some(19_690_720) = device.execute() {
                return Ok(100 * noun + verb);
            }
        }
    }

    Err("Cannot find valid noun-verb".into())
}

#[derive(Clone)]
struct IntcodeDevice {
    memory: Vec<usize>
}

impl IntcodeDevice {
    fn execute(mut self) -> Option<usize> {
        self.helper_execute(0)
    }

    fn helper_execute(&mut self, ip: usize) -> Option<usize> {
        let opcode = *self.memory.get(4 * ip)?;
        if opcode == 99 {
            return self.memory.get(0).cloned();
        }

        let op1_addr = *self.memory.get(4 * ip + 1)?;
        let op2_addr = *self.memory.get(4 * ip + 2)?;
        let store_addr = *self.memory.get(4 * ip + 3)?;

        if store_addr >= self.memory.len() {
            // Store values outside the memory is forbidden
            return None;
        }

        self.memory[store_addr] = match opcode {
            1 => self.memory.get(op1_addr)? + self.memory.get(op2_addr)?,
            2 => self.memory.get(op1_addr)? * self.memory.get(op2_addr)?,
            // Invalid opcode found, the input is invalid and there's no valid answer
            _ => return None,
        };

        self.helper_execute(ip + 1)
    }
}

impl FromStr for IntcodeDevice {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let memory = s
            .split(',')
            .map(|n|
                n.parse::<usize>()
                    .map_err(|_| format!("Cannot parse \"{}\" as a positive integer", n))
            )
            .collect::<std::result::Result<Vec<_>, _>>()?;

        if memory.len() < 4 {
            return Err("Input length too short".into());
        }

        Ok(Self { memory })
    }
}