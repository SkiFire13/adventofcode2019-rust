mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<i64> {
    let mut device: IntcodeDevice = input.parse()?;
    device.input.push_back(1);
    device.execute()?;
    device.output.pop().ok_or_else(|| "Error during execution".into())
}

fn part2(input: &str) -> Result<i64> {
    let mut device: IntcodeDevice = input.parse()?;
    device.input.push_back(2);
    device.execute()?;
    device.output.pop().ok_or_else(|| "Error during execution".into())
}