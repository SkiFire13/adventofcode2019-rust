mod intcode;

use std::collections::HashMap;
use std::fs::read_to_string;
use std::path::Path;
use self::intcode::{DeviceStatus, IntcodeDevice};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let mut device: IntcodeDevice = input.parse()?;

    let mut panels = HashMap::new();
    let mut position = (0isize, 0isize);
    let mut direction = (0isize, 1isize);

    loop { 
        device.input.push_back( if let Some(Color::White) = panels.get(&position) { 1 } else { 0 });
        let next_result = device.execute();
        match next_result {
            Ok(DeviceStatus::Halt) => return Ok(panels.len()),
            Ok(DeviceStatus::WaitingInput) => {
                direction = match device.output.pop() {
                    // Complex point rotation; multiply by i * delta
                    Some(0) => (-direction.1, direction.0),
                    Some(1) => (direction.1, -direction.0),
                    _ => return Err("Invalid direction output from VM".into())
                };
                let new_color = match device.output.pop() {
                    Some(0) => Color::Black,
                    Some(1) => Color::White,
                    _ => return Err("Invalid color output from VM".into())
                };
                panels.insert(position, new_color);
                position = (position.0 + direction.0, position.1 + direction.1);
            }
            Err(e) => return Err(e)
        }
    }
}

fn part2(input: &str) -> Result<String> {
    let mut device: IntcodeDevice = input.parse()?;

    let mut panels = HashMap::new();
    let mut position = (0isize, 0isize);
    let mut direction = (0isize, 1isize);

    panels.insert(position, Color::White);

    loop { 
        device.input.push_back( if let Some(Color::White) = panels.get(&position) { 1 } else { 0 });
        let next_result = device.execute();
        match next_result {
            Ok(DeviceStatus::Halt) => break,
            Ok(DeviceStatus::WaitingInput) => {
                direction = match device.output.pop() {
                    // Complex point rotation; multiply by i * delta
                    Some(0) => (-direction.1, direction.0),
                    Some(1) => (direction.1, -direction.0),
                    _ => return Err("Invalid direction output from VM".into())
                };
                let new_color = match device.output.pop() {
                    Some(0) => Color::Black,
                    Some(1) => Color::White,
                    _ => return Err("Invalid color output from VM".into())
                };
                panels.insert(position, new_color);
                position = (position.0 + direction.0, position.1 + direction.1);
            }
            Err(e) => return Err(e)
        }
    }

    let mut output = String::new();
    let (minx, maxx, miny, maxy) = panels.keys()
        .fold((None, None, None, None), |(minx, maxx, miny, maxy), &(x, y)|
            (
                Some(std::cmp::min(minx.unwrap_or(x), x)),
                Some(std::cmp::max(maxx.unwrap_or(x), x)),
                Some(std::cmp::min(miny.unwrap_or(y), y)),
                Some(std::cmp::max(maxy.unwrap_or(y), y)),
            )
        );
    
    let minx = minx.ok_or_else(|| "No panels")?;
    let maxx = maxx.ok_or_else(|| "No panels")?;
    let miny = miny.ok_or_else(|| "No panels")?;
    let maxy = maxy.ok_or_else(|| "No panels")?;
    
    for y in (miny..=maxy).rev() {
        output.push('\n');
        for x in minx..=maxx {
            match panels.get(&(x,y)) {
                Some(Color::White) => output.push('\u{2588}'),
                _ => output.push(' '),
            }
        }
    }

    Ok(output)
}

enum Color { Black, White }
