mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<i64> {
    let mut device: IntcodeDevice = input.parse()?;

    device.input.extend(
        "\
        NOT A J\n\
        NOT B T\n\
        OR T J\n\
        NOT C T\n\
        OR T J\n\
        AND D J\n\
        WALK\n\
        ".chars().map(|c| c as u8 as i64) 
    );

    device.execute()?;

    match device.output.pop_back() {
        Some(answer) if answer > 255 => Ok(answer),
        Some(_) => Err("Didn't make it across".into()),
        None => Err("No output from device".into()),
    }
}

fn part2(input: &str) -> Result<i64> {
    let mut device: IntcodeDevice = input.parse()?;

    device.input.extend(
        "\
        NOT A J\n\
        NOT B T\n\
        OR T J\n\
        NOT C T\n\
        OR T J\n\
        AND D J\n\
        NOT E T\n\
        NOT T T\n\
        OR H T\n\
        AND T J\n\
        RUN\n\
        ".chars().map(|c| c as u8 as i64) 
    );

    device.execute()?;

    match device.output.pop_back() {
        Some(answer) if answer > 255 => Ok(answer),
        Some(_) => Err("Didn't make it across".into()),
        None => Err("No output from device".into()),
    }
}
