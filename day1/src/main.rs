use std::fs::read_to_string;
use std::iter::successors;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<u32> {
    input.lines()
        .map(|l| 
            l.parse::<u32>()
                .map(|m| (m / 3).saturating_sub(2))
                .map_err(|_| format!("Input \"{}\" is not a positive integer", l).into())
        )
        .sum()
}

fn part2(input: &str) -> Result<u32> {
    input.lines()
        .map(|l| 
            l.parse::<u32>()
                .map(|m|
                    successors(Some(m), |&m| (m / 3).checked_sub(2))
                        .skip(1)
                        .sum::<u32>()
                )
                .map_err(|_| format!("Input \"{}\" is not a positive integer", l).into())
        )
        .sum()
}
