mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<i32> {
    let mut device: IntcodeDevice = input.parse()?;
    device.input.push_back(1);
    device.execute()?;
    if device.output.iter().rev().skip(1).all(|&i| i == 0) {
        device.output.pop().ok_or_else(|| "No output".into())
    } else {
        Err(format!("Some checks failed: {:?}", device.output).into())
    }
}

fn part2(input: &str) -> Result<i32> {
    let mut device: IntcodeDevice = input.parse()?;
    device.input.push_back(5);
    device.execute()?;
    if device.output.iter().rev().skip(1).all(|&i| i == 0) {
        device.output.pop().ok_or_else(|| "No output".into())
    } else {
        Err(format!("Some checks failed: {:?}", device.output).into())
    }
}
