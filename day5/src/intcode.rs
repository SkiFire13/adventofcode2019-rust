use std::collections::VecDeque;
use std::convert::{TryFrom, TryInto};
use std::str::FromStr;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Clone)]
pub struct IntcodeDevice {
    ip: usize,
    memory: Vec<i32>,
    pub input: VecDeque<i32>,
    pub output: Vec<i32>,
}

impl IntcodeDevice {
    pub fn execute(&mut self) -> Result<DeviceStatus> {
        loop {
            let opcode = {
                let opcode = self.memory.get(self.ip).cloned().ok_or("No more instructions")?;
                if opcode > 0 { opcode as usize } else { return Err("Negative opcode".into()); }
            };

            let param1 = self.memory.get(self.ip + 1).cloned()
                .ok_or_else(|| format!("Param 1 is required for {}", opcode));
            let param2 = self.memory.get(self.ip + 2).cloned()
                .ok_or_else(|| format!("Param 2 is required for {}", opcode));
            let param3 = self.memory.get(self.ip + 3).cloned()
                .ok_or_else(|| format!("Param 3 is required for {}", opcode));

            use ParamMode::*;
            match Self::parse_opcode(opcode)? {
                (99, [Position, Position, Position]) => return Ok(DeviceStatus::Halt),
                (1, [mode1, mode2, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?)?;
                    self.memory[result_addr] = param1 + param2;
                    self.ip += 4;
                },
                (2, [mode1, mode2, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?)?;
                    self.memory[result_addr] = param1 * param2;
                    self.ip += 4;
                },
                (3, [Position, Position, Position]) => {
                    let result_addr = self.check_address(param1?)?;
                    if let Some(input_value) = self.input.pop_front() {
                        self.memory[result_addr] = input_value;
                        self.ip += 2;
                    } else {
                        return Ok(DeviceStatus::WaitingInput)
                    }
                },
                (4, [mode1, Position, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    self.output.push(param1);
                    self.ip += 2;
                },
                (5, [mode1, mode2, _]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    if param1 != 0 {
                        self.ip = self.check_address(self.get_param(param2?, mode2)?)?;
                    } else {
                        self.ip += 3;
                    }
                },
                (6, [mode1, mode2, _]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    if param1 == 0 {
                        self.ip = self.check_address(self.get_param(param2?, mode2)?)?;
                    } else {
                        self.ip += 3;
                    }
                },
                (7, [mode1, mode2, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?)?;
                    self.memory[result_addr] = if param1 < param2 { 1 } else { 0 };
                    self.ip += 4;
                },
                (8, [mode1, mode2, Position]) => {
                    let param1 = self.get_param(param1?, mode1)?;
                    let param2 = self.get_param(param2?, mode2)?;
                    let result_addr = self.check_address(param3?)?;
                    self.memory[result_addr] = if param1 == param2 { 1 } else { 0 };
                    self.ip += 4;
                },
                (op_code, modes) => return Err(format!("Invalid opcode {} with modes {:?}", op_code, modes).into())
            }
        }
    }

    fn parse_opcode(opcode: usize) -> Result<(usize, [ParamMode ; 3])> {
        Ok((
            opcode % 100,
            [
                (opcode / 100 % 10).try_into()?,
                (opcode / 1000 % 10).try_into()?,
                (opcode / 10000 % 10).try_into()?
            ]
        ))
    }

    fn check_address(&self, address: i32) -> Result<usize> {
        address.try_into()
            .ok()
            .filter(|&address| address < self.memory.len())
            .ok_or_else(|| format!("Address {} out of bounds", address).into())
    }

    fn get_param(&self, param: i32, mode: ParamMode) -> Result<i32> {
        match mode {
            ParamMode::Immediate => Ok(param),
            ParamMode::Position => self.check_address(param).ok()
                .and_then(|addr| self.memory.get(addr).cloned())
                .ok_or_else(|| "Parameter address out of bounds".into())
        }
    }
}

impl FromStr for IntcodeDevice {
    type Err = String;
    fn from_str(input: &str) -> std::result::Result<Self, Self::Err> {
        Ok(Self {
            ip: 0,
            input: VecDeque::new(),
            output: Vec::new(),
            memory: input
                .trim()
                .split(',')
                .map(|n| n.parse::<i32>()
                    .map_err(|_| format!("Cannot parse {} as a positive integer", n))
                )
                .collect::<std::result::Result<Vec<_>, _>>()?
        })
    }
}

#[derive(Debug)]
enum ParamMode { Immediate, Position }
impl TryFrom<usize> for ParamMode {
    type Error = Box<dyn std::error::Error>;
    fn try_from(mode: usize) -> Result<Self> {
        match mode {
            0 => Ok(Self::Position),
            1 => Ok(Self::Immediate),
            _ => Err("Invalid parameter mode in opcode".into())
        }
    }
}

pub enum DeviceStatus { Halt, WaitingInput }
