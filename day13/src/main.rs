mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let mut device: IntcodeDevice = input.parse()?;

    let mut blocks_count = 0;

    device.execute()?;
    while !device.output.is_empty() {
        let _ = device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")?;
        let _ = device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")?;
        match device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")? {
            0 /* Empty */ | 1 /* Wall */ | 3 /* Paddle */ | 4 /* Ball */ => {},
            2 /* Block */ => blocks_count += 1,
            _ => return Err("Invalid tile type".into())
        };
    }

    Ok(blocks_count)
}

fn part2(input: &str) -> Result<i64> {
    let mut device: IntcodeDevice = input.parse()?;
    device.memory[0] = 2;

    let mut blocks_count = 0;
    let mut first = true;

    let mut ball_x = 0;
    let mut paddle_x = 0;

    let mut score = 0;

    loop {
        device.execute()?;
        while !device.output.is_empty() {
            let x = device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")?;
            let y = device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")?;
            if x == -1 && y == 0 {
                score = device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")?;
            } else {
                match device.output.pop_front().ok_or_else(|| "Invalid output from intcode device")? {
                    0 /* Empty */ => if !first { blocks_count -= 1; },
                    1 /* Wall */ => {},
                    2 /* Block */ => if first { blocks_count += 1; },
                    3 /* Paddle */ => paddle_x = x,
                    4 /* Ball */ => {
                        if !first { blocks_count += 2; }
                        ball_x = x;
                    },
                    _ => return Err("Invalid tile type".into())
                }
            }
        }

        device.input.push_back(match ball_x.cmp(&paddle_x) {
            std::cmp::Ordering::Less => -1,
            std::cmp::Ordering::Equal => 0,
            std::cmp::Ordering::Greater => 1,
        });

        if blocks_count == 0 {
            return Ok(score)
        }
        first = false;
    }
}
