use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let (_, depends_on) = parse_input(input)?;
    fn num_orbits(node: &str, depends_on: &HashMap<&str, Vec<&str>>) -> usize {
        depends_on.get(node).map_or(0, |node_deps|
            node_deps.len() + node_deps.iter().map(|sub_node| num_orbits(sub_node, depends_on)).sum::<usize>()
        )
    }
    Ok(depends_on.keys().map(|k| num_orbits(k, &depends_on)).sum())
}

fn part2(input: &str) -> Result<i32> {
    let (depended_by, depends_on) = parse_input(input)?;
    let mut seen = HashSet::new();
    let mut queue = BinaryHeap::new();
    queue.push(Reverse((0, "YOU")));
    loop {
        let Reverse((dist, node)) = queue.pop().ok_or_else(|| "There's no path from YOU to SAN")?;
        if seen.contains(node) { continue; }
        if node == "SAN" { return Ok(dist - 2); }
        seen.insert(node);
        depended_by.get(node).into_iter().flatten()
            .chain(depends_on.get(node).into_iter().flatten())
            .for_each(|sub_node| queue.push(Reverse((dist + 1, sub_node))))
    }
}

fn parse_input(input: &str) -> Result<(HashMap<&str, Vec<&str>>, HashMap<&str, Vec<&str>>)> {
    let mut depended_by = HashMap::new();
    let mut depends_on = HashMap::new();
    for line in input.lines() {
        let mut split = line.split(')');
        let dependency = split.next().ok_or_else(|| format!("Invalid input line {}", line))?;
        let depend = split.next().ok_or_else(|| format!("Invalid input line {}", line))?;
        if split.next().is_some() { return Err(format!("Invalid input line {}", line).into());}
        depended_by.entry(dependency).or_insert_with(Vec::new).push(depend);
        depends_on.entry(depend).or_insert_with(Vec::new).push(dependency);
    }
    Ok((depended_by, depends_on))
}
