use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let (_, o, t) = input.trim_end()
        .as_bytes()
        .chunks(25 * 6)
        .map(|sub_str| (
            sub_str.iter().filter(|&&b| b==b'0').count(),
            sub_str.iter().filter(|&&b| b==b'1').count(),
            sub_str.iter().filter(|&&b| b==b'2').count())
        )
        .min()
        .ok_or_else(|| "Invalid input")?;
    Ok(o * t)
}

fn part2(input: &str) -> Result<String> {
    let input = input.trim_end().as_bytes();
    Ok(
        (0..25*6)
            .map(|pos| get_pixel(&input, pos))
            .map(|c| match c {
                b'0' => ' ',
                b'1' => '\u{2588}',
                _ => '?',
            })
            .enumerate()
            .fold(String::new(), |mut s, (i, c)| {
                if i % 25 == 0 { s.push('\n') }
                s.push(c);
                s
            })
    )
}

fn get_pixel(input: &[u8], pos: usize) -> u8 {
    match input.get(pos) {
        None => b'2',
        Some(b'2') => get_pixel(&input[25*6..], pos),
        Some(&b) => b,
    }
}