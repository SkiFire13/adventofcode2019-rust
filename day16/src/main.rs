use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<u32> {
    let mut numbers = input.trim().chars()
        .map(|c| c.to_digit(10).ok_or_else(|| "Invalid character".into()))
        .collect::<Result<Vec<_>>>()?;
    let mut temp_vec = Vec::new();

    for _ in 0..100 {
        temp_vec.clear();
        temp_vec.extend(
            (1..=numbers.len()).map(|i|
                numbers.iter()
                    .zip(pattern_iter(i).skip(1))
                    .map(|(d, p)| (*d as i32) * p)
                    .sum::<i32>().abs() as u32
                    % 10
            )
        );
        std::mem::swap(&mut numbers, &mut temp_vec);
    }

    Ok(numbers[..8].iter().fold(0, |sum, n| 10 * sum + n))
}

fn part2(input: &str) -> Result<u32> {
    let input = input.trim().chars()
        .map(|c| c.to_digit(10).ok_or_else(|| "Invalid character".into()))
        .collect::<Result<Vec<_>>>()?;

    let message_offset = input[..7].iter().fold(0, |sum, n| 10 * sum + n) as usize;
    if 2 * message_offset - 1 < input.len() { return Err("This is impossible in human time".into()); }

    let mut numbers = input.iter().cloned()
        .cycle()
        .skip(message_offset % input.len())
        .take(10000 * input.len() - message_offset)
        .collect::<Vec<_>>();

    for _ in 0..100 {
        for i in (0..numbers.len()-1).rev() {
            numbers[i] = (numbers[i + 1] + numbers[i]) % 10;
        }
    }

    Ok(numbers[..8].iter().fold(0, |sum, n| 10 * sum + n))
}

fn pattern_iter(n: usize) -> impl Iterator<Item = i32> {
    [0, 1, 0, -1].into_iter()
        .flat_map(move |d| std::iter::repeat(d).take(n))
        .cloned()
        .cycle()
}