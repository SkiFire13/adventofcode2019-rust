mod intcode;

use std::collections::{BinaryHeap, HashMap, HashSet};
use std::cmp::Reverse;
use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<u32> {
    let (map, oxygen_pos) = explore(input)?;

    let mut queue = BinaryHeap::new();
    let mut seen = HashSet::new();

    queue.push(Reverse((0, (0, 0))));
    loop {
        let Reverse((dist, pos)) = queue.pop().unwrap();
        if seen.insert(pos) {
            if pos == oxygen_pos {
                return Ok(dist)
            }
            for near_pos in near(pos, &map) {
                queue.push(Reverse((dist+1, near_pos)));
            }
        }
    }
}

fn part2(input: &str) -> Result<u32> {
    let (map, oxygen_pos) = explore(input)?;

    let mut queue = BinaryHeap::new();
    let mut seen = HashSet::new();

    queue.push(Reverse((0, oxygen_pos)));
    
    let mut max = 0;

    loop {
        if let Some(Reverse((dist, pos))) = queue.pop() {
            if seen.insert(pos) {
                max = dist;
                for near_pos in near(pos, &map) {
                    queue.push(Reverse((dist+1, near_pos)));
                }
            }
        } else {
            return Ok(max)
        }
    }
}

fn explore(input: &str) -> Result<(HashMap<(i32, i32), MapStatus>, (i32, i32))> {
    let mut device: IntcodeDevice = input.parse()?;
    let mut map = HashMap::new();
    let mut path = Vec::new();

    path.push((0, 0));
    map.insert((0, 0), MapStatus::Empty);

    let mut oxygen_pos = Err("No oxygen found");

    loop {
        let pos = *path.last().unwrap();
        
        let dest = near(pos, &map).filter(|p| map.get(p) == None).next().or_else(|| {
            path.pop(); // Go back
            path.pop()
        });
        let dest = if let Some(dest) = dest { dest } else { return Ok((map, oxygen_pos?)) };

        device.input.push_back(match (dest.0 - pos.0, dest.1 - pos.1) {
            (0, 1) => 1,
            (0, -1) => 2,
            (1, 0) => 3,
            (-1, 0) => 4,
            _ => unreachable!(),
        });
        device.execute()?;
        let map_type = match device.output.pop_front() {
            Some(0) => MapStatus::Wall,
            Some(1) => MapStatus::Empty,
            Some(2) => MapStatus::Oxygen,
            _ => return Err("Invalid device output".into())
        };
        if map_type == MapStatus::Oxygen { oxygen_pos = Ok(dest); }
        if map_type != MapStatus::Wall { path.push(dest); }
        map.insert(dest, map_type);
    }
}

fn near(pos: (i32, i32), map: &HashMap<(i32, i32), MapStatus>) -> impl Iterator<Item = (i32, i32)> + '_ {
    [(1, 0), (-1, 0), (0, 1), (0, -1)].into_iter()
        .map(move |(dx, dy)| (pos.0 + dx, pos.1 + dy))
        .filter(move |new_pos| map.get(new_pos) != Some(&MapStatus::Wall))
}

#[derive(PartialEq, Eq)]
enum MapStatus { Wall, Empty, Oxygen }
