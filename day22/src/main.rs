use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<i128> {
    let size: i128 = 10007;
    let mut idx: i128 = 2019;
    let istrs = parse_actions(&input)?;

    for istr in istrs {
        idx = match istr {
            Action::Cut(cut) => idx - cut,
            Action::Stack => -idx - 1,
            Action::Increment(increment) =>  increment * idx,
        }.rem_euclid(size);
    }

    Ok(idx)
}

fn part2(input: &str) -> Result<i128> {
    let size: i128 = 119315717514047;
    let n_iter: i128 = 101741582076661;
    let idx: i128 = 2020;
    let istrs = parse_actions(&input)?;

    let mut coeffs = (1, 0);
    for istr in istrs.iter().rev() {
        coeffs = match istr {
            Action::Cut(cut) => (coeffs.0, coeffs.1 + cut),
            Action::Stack => (-coeffs.0, -coeffs.1-1),
            Action::Increment(increment) => {
                let inv = mod_inv(*increment, size);
                (coeffs.0 * inv, coeffs.1 * inv)
            }
        };
        coeffs = (coeffs.0.rem_euclid(size), coeffs.1.rem_euclid(size));
    }
    let (a, b) = coeffs;
    let an = mod_pow(a, n_iter, size);

    Ok((an * idx % size + (an - 1) * mod_inv(a - 1, size) % size * b % size) % size)
}

fn mod_pow(mut base: i128, mut exp: i128, modulus: i128) -> i128 {
    let mut result = 1;
    base = base % modulus;
    while exp > 0 {
        if exp % 2 == 1 {
            result = result * base % modulus;
        }
        exp = exp >> 1;
        base = base * base % modulus
    }
    result
}

fn mod_inv(a: i128, module: i128) -> i128 {
    let mut mn = (module, a);
    let mut xy = (0, 1);
   
    while mn.1 != 0 {
        xy = (xy.1, xy.0 - (mn.0 / mn.1) * xy.1);
        mn = (mn.1, mn.0 % mn.1);
    }
   
    while xy.0 < 0 {
        xy.0 += module;
    }
    
    xy.0
}

enum Action {
    Cut(i128),
    Stack,
    Increment(i128)
}

fn parse_actions(input: &str) -> Result<Vec<Action>> {
    input.lines()
        .map(|line|
            if line.starts_with("deal with increment ") {
                line[20..].parse()
                    .map(|increment| Action::Increment(increment))
                    .map_err(|_| "Can't parse increment amount".into())
            } else if line.starts_with("cut ") {
                line[4..].parse()
                    .map(|cut| Action::Cut(cut))
                    .map_err(|_| "Can't parse cut amount".into())
            } else if line == "deal into new stack" {
                Ok(Action::Stack)
            } else {
                Err("Can't match input line".into())
            }
        )
        .collect::<Result<Vec<_>>>()
}