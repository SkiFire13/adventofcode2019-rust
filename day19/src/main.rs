mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::IntcodeDevice;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<u32> {
    let base_device: IntcodeDevice = input.parse()?;
    let mut device = base_device.clone();

    (0..50)
        .flat_map(|y| (0..50).map(move |x| (x,y)))
        .map(|(x,y)| is_attracted(&mut device, &base_device, x, y).map(|b| b as u32))
        .sum::<Result<u32>>()
}

fn part2(input: &str) -> Result<i64> {
    let base_device: IntcodeDevice = input.parse()?;
    let mut device = base_device.clone();

    let (mut x, mut y) = (0, 0);

    loop {
        if !is_attracted(&mut device, &base_device, x, y + 99)? {
            x += 1;
            continue;
        }
        if !is_attracted(&mut device, &base_device, x + 99, y)? {
            y += 1;
            continue;
        }
        return Ok(10000 * x + y)
    }
}

fn is_attracted(device: &mut IntcodeDevice, base_device: &IntcodeDevice, x: i64, y: i64) -> Result<bool> {
    device.reset(&base_device);
    device.input.push_back(x);
    device.input.push_back(y);
    device.execute()?;
    match device.output.pop_back().ok_or_else(|| "Intcode device didn't report anything")? {
        0 => Ok(false),
        1 => Ok(true),
        _ => Err("Invalid report from intcode device".into())
    }
}
