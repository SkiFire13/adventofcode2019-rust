mod intcode;

use std::fs::read_to_string;
use std::path::Path;
use self::intcode::{DeviceStatus, IntcodeDevice};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<i32> {
    let device: IntcodeDevice = input.parse()?;
    let mut max = std::i32::MIN;
    for (a,b,c,d,e) in permutations_iter() {
        let mut input = 0;
        for n in &[a,b,c,d,e] {
            let mut device = device.clone();
            device.input.push_back(*n);
            device.input.push_back(input);
            device.execute()?;
            input = device.output.pop().ok_or_else(|| "No output")?;
        }
        max = if input > max { input } else { max };
    }
    Ok(max)
}

fn part2(input: &str) -> Result<i32> {
    let device: IntcodeDevice = input.parse()?;
    let mut max = std::i32::MIN;

    for (a,b,c,d,e) in permutations_iter() {
        let mut devices = [device.clone(), device.clone(), device.clone(), device.clone(), device.clone()];
        for (device, phase) in devices.iter_mut().zip([a,b,c,d,e].iter()) {
            device.input.push_back(phase+5);
        }
        let mut buffer: Vec<i32> = vec![0];
        let output = 'output: loop {
            for (i, device) in devices.iter_mut().enumerate() {
                device.input.append(&mut buffer.into());
                let execute_result = device.execute();
                buffer = device.output.drain(..).collect();
                if let Ok(DeviceStatus::Halt) = execute_result {
                    if i == 4 {
                        break 'output buffer.get(0).cloned().ok_or_else(|| "No output")?;
                    }
                }
            }
        };
        max = if output > max { output } else { max };
    }
    Ok(max)
}

fn permutations_iter() -> impl Iterator<Item = (i32, i32, i32, i32, i32)> {
    (0..5)
        .flat_map(|a| (0..5).filter(move|&b| b != a).map(move|b| (a,b)))
        .flat_map(|(a,b)| (0..5).filter(move|&c| c != b && c != a).map(move|c|(a,b,c)))
        .flat_map(|(a,b,c)| (0..5).filter(move|&d| d != a && d != b && d != c).map(move|d|(a,b,c,d)))
        .flat_map(|(a,b,c,d)| (0..5).filter(move|&e| e != a && e != b && e != c && e != d).map(move|e|(a,b,c,d,e)))
}
