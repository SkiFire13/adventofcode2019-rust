use std::collections::HashMap;
use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<u32> {
    intersections(input)?
        .map(|((x,y), _)| (x.abs() + y.abs()) as u32)
        .min()
        .ok_or_else(|| "There are no intersections in the paths".into())
}

fn part2(input: &str) -> Result<u32> {
    intersections(input)?
        .map(|(_, v)| v)
        .min()
        .ok_or_else(|| "There are no intersections in the paths".into())
}

fn intersections(input: &str) -> Result<impl Iterator<Item = ((i32, i32), u32)>> {
    let mut paths = Vec::new();

    for line in input.lines() {
        let mut current = (0i32, 0i32);
        let mut points = HashMap::new();
        let mut distance = 0u32;

        for segment in line.split(',') {
            let delta = match segment.chars().next() {
                Some('R') => (1, 0),
                Some('L') => (-1, 0),
                Some('U') => (0, 1),
                Some('D') => (0, -1),
                _ => return Err("Invalid input".into())
            };

            let length: usize = segment[1..]
                .parse()
                .map_err(|_|
                    format!("Cannot parse \"{}\" as a number", &segment[1..])
                )?;

            for _ in 0..length {
                distance += 1;
                current = (current.0 + delta.0, current.1 + delta.1);
                points.entry(current).or_insert(distance);
            }
        }
        paths.push(points);
    }
    
    if paths.len() != 2 {
        return Err("Input has a wrong number of paths".into());
    }
    
    let second_path = paths.remove(1);
    let first_path = paths.remove(0);
    Ok(
        first_path
            .into_iter()
            .filter_map(move |(k, v1)|
                second_path.get(&k).map(|v2| (k, v1 + v2))
            )
    )
}
