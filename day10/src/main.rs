use std::collections::HashSet;
use std::fs::read_to_string;
use std::path::Path;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let input = {
        let input_file = Path::new(env!("CARGO_MANIFEST_DIR")).join("input").join("input");
        read_to_string(&input_file)
            .map_err(|_| format!("Cannot find input file {}", input_file.display()))?
    };

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &str) -> Result<usize> {
    let asteroids = parse_input(input);
    asteroids
        .iter()
        .map(|&asteroid| seen_by(&asteroids, asteroid).count())
        .max()
        .ok_or_else(|| "No asteroids".into())
}

fn part2(input: &str) -> Result<isize> {
    let mut asteroids = parse_input(input);

    let station = *asteroids
        .iter()
        .max_by_key(|&&asteroid| seen_by(&asteroids, asteroid).count())
        .ok_or_else(|| "No asteroids")?;
    asteroids.remove(&station);
    
    let mut last_removed_angle = std::f64::consts::FRAC_PI_2;
    let mut count = 0;

    let first = asteroids.iter()
        .map(|&asteroid| station - asteroid)
        .filter(|&Asteroid { x, y }| x == 0 && y > 0)
        .min_by_key(|&asteroid| asteroid.y);
    if let Some(first) = first {
        let target = station - first;
        asteroids.remove(&target);
        count += 1;
    }

    loop {
        let (target, target_angle) = asteroids.iter()
            .map(|&asteroid| station - asteroid)
            .map(|asteroid| (asteroid, (asteroid.y as f64).atan2(asteroid.x as f64)))
            .min_by(|&(ast1, mut a1), &(ast2, mut a2)| {
                while a1 <= last_removed_angle { a1 += 2.0 * std::f64::consts::PI; }
                while a2 <= last_removed_angle { a2 += 2.0 * std::f64::consts::PI; }
                a1.partial_cmp(&a2).unwrap_or(std::cmp::Ordering::Equal)
                    .then(ast1.dist().cmp(&ast2.dist()))
            })
            .ok_or_else(|| "There are less than 200 asteroids")?;
        
        let target = station - target;
        
        last_removed_angle = target_angle;
        asteroids.remove(&target);
        count += 1;

        if count == 200 {
            return Ok(100 * target.x + target.y)
        }
    }
}

fn parse_input(input: &str) -> HashSet<Asteroid> {
    input.lines()
        .enumerate()
        .flat_map(|(y, line)|
            line.chars()
                .enumerate()
                .filter(|&(_, c)| c == '#')
                .map(move |(x, _)| Asteroid { x: x as isize, y: y as isize })
        )
        .collect()
}

fn seen_by(asteroids: &HashSet<Asteroid>, station: Asteroid) -> impl Iterator<Item = Asteroid> + '_ {
    asteroids.iter()
        .filter(move |&&a| a != station)
        .map(move |&asteroid| asteroid - station)
        .filter(move |&Asteroid { mut x, mut y }| {
            let gcd = gcd(x.abs(), y.abs());
            x = x / gcd;
            y = y / gcd;
            (1..gcd).all(|gcd| !asteroids.contains(&Asteroid { x: station.x + x * gcd, y: station.y + y * gcd }))
        })
}

fn gcd(mut x: isize, mut y: isize) -> isize {
    if x == 0 { return y }
    if y == 0 { return x }
    while y != 0 {
        std::mem::swap(&mut x, &mut y);
        y = y % x;
    }
    return x
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
struct Asteroid { x: isize, y: isize}
impl Asteroid {
    fn dist(&self) -> isize {
        self.x * self.x + self.y * self.y
    }
}
impl std::ops::Sub for Asteroid {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self { x: self.x - other.x, y: self.y - other.y }
    }
}